# Exercises node JS

## Getting started
To start clone this project with ```https://gitlab.com/MatthieuBrehamel/brehamel-my-exercices.git```

You need to run ```npm install```

If you use 'dico' type ```node exercise(number).js```

To test an exercise


## Resume

# Exercise 0 :
Give a number to a and b to my_sum(a,b) function and she return the result of a + b

# Exercise 1 :
The function my_display_alpha() return all letters of alphabet in lowercase

# Exercise 2 :
The function my_display_alpha_reverse() return all letters of alphabet in lowercase but reverse

# Exercise 3 :
The function my_alpha_number(nbr) return nbr to string

# Exercise 4 :
The function my_size_alpha(str) count number of letters in str passed

# Exercise 5 :
The function my_array_alpha(str) return an array off all letters in str

# Exercise 6 :
The function my_length_array(arr) count all elements in the arrayy passed

# Exercise 7 :
The function my_is_posi_neg(nbr) return POSITIF if nbr is positive, null or undefined,
return NEUTRAL if nbr is zero and return NEGATIF if nbr is negative

## Technology
- Node Js 
- Mocha
- Chai
