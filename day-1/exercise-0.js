// Enter two numbers a and b in my_sum function and she return a + b
export const my_sum = (a, b) => {
  if (parseInt(a) === 0 || parseInt(b) === 0) {
    return 0;
  } else if (typeof a !== "number" || typeof b !== "number") {
    return 0;
  } else {
    return parseInt(a) + parseInt(b);
  }
};
