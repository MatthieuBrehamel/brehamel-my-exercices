// function my_display_alpha return alphabet in lower case
export const my_display_alpha = () => {
  let alphabet = "";
  let i = 97;

  for (i; i <= 122; ++i) {
    alphabet = alphabet + String.fromCharCode(i);
  }

  return alphabet;
};
