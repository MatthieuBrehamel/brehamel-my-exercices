// function my_display_alpha_reverse return reverse alphabet in lower case
export const my_display_alpha_reverse = () => {
  let alphabet = "";
  let i = 122;

  for (i; i >= 97; --i) {
    alphabet = alphabet + String.fromCharCode(i);
  }

  return alphabet;
};
