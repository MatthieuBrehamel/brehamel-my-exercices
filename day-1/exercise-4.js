// my_size_alpha function count number of letter passed
export const my_size_alpha = (str) => {
  let i = 0;

  while (str[i]) {
    i++;
  }

  return i;
};
