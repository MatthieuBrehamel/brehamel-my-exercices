// Function my_array_alpha return array of string passed in argument
export const my_array_alpha = (str) => {
  let i = 0;
  let array = [];

  while (str[i]) {
    array[i] = str[i];
    i++;
  }

  return array;
};
