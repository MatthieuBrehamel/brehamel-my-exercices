// Function my_length_array count all elements in the array passed in argument
export const my_length_array = (arr) => {
  let count = 0;

  for (let item in arr) {
    count++;
  }

  return count;
};
