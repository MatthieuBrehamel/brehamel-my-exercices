// Function my_is_posi_neg return POSITIF, NEGATIF or NEUTRAL
export const my_is_posi_neg = (nbr) => {
  let testInt = Math.sign(nbr);

  if (testInt >= 1 || isNaN(testInt) || nbr === null) {
    return "POSITIF";
  } else if (testInt === 0) {
    return "NEUTRAL";
  } else {
    return "NEGATIF";
  }
};
