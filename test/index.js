import assert from 'assert';
import {expect} from 'chai';
import {my_sum} from "../day-1/exercise-0.js";
import {my_display_alpha} from "../day-1/exercise-1.js";
import {my_display_alpha_reverse} from "../day-1/exercise-2.js";
import {my_alpha_number} from "../day-1/exercise-3.js";
import {my_size_alpha} from "../day-1/exercise-4.js";
import {my_array_alpha} from "../day-1/exercise-5.js";
import {my_length_array} from "../day-1/exercise-6.js";
import {my_is_posi_neg} from "../day-1/exercise-7.js";

//Exercice 0
describe('test exercice 0', () => {
  it('should return 3 when passed a = 1 and b = 2', function(done) {
    let result = my_sum(1,2);
    assert.equal(result, 3, 'resultat correct');

    done();
  });
});
describe('test exercice 0', () => {
  it('should return 0 when passed a = 1 and b =  ', function (done) {
    let result = my_sum(1);
    assert.equal(result, 0, 'resultat correct');

    done();
  });
});
describe('test exercice 0', () => {
  it('should return 0 when passed a = "1" and b = 1', function (done) {
    let result = my_sum("1",1);
    assert.equal(result, 0, 'resultat correct');

    done();
  });
});
describe('test exercice 0', () => {
  it('should return 0 when passed a = 1 and b = "1"', function (done) {
    let result = my_sum(1, "1");
    assert.equal(result, 0, 'resultat correct');

    done();
  });
});

//Exercice 1
describe('test exercice 1', () => {
  it('should return abcdefghijklmnopqrstuvwxyz', function (done) {
    let result = my_display_alpha();
    assert.equal(result, 'abcdefghijklmnopqrstuvwxyz', 'resultat correct');

    done();
  });
});

//Exercice 2
describe('test exercice 2', () => {
  it('should return zyxwvutsrqponmlkjihgfedcba', function (done) {
    let result = my_display_alpha_reverse();
    assert.equal(result, 'zyxwvutsrqponmlkjihgfedcba', 'resultat correct');

    done();
  });
});

//Exercice 3
describe('test exercice 3', () => {
  it('should return "1" when passed nbr = 1', function (done) {
    let result = my_alpha_number(1);
    assert.equal(result, '1', 'resultat correct');

    done();
  });
});
describe('test exercice 3', () => {
  it('should return "1" when passed nbr = "1"', function (done) {
    let result = my_alpha_number("1");
    assert.equal(result, '1', 'resultat correct');

    done();
  });
});
describe('test exercice 3', () => {
  it('should return "27682697209" when passed nbr = 27682697209', function (done) {
    let result = my_alpha_number(27682697209);
    assert.equal(result, 27682697209, 'resultat correct');

    done();
  });
});

//Exercice 4
describe('test exercice 4', () => {
  it('should return 11 when passed str = "27682697209"', function (done) {
    let result = my_size_alpha("27682697209");
    assert.equal(result, 11, 'resultat correct');

    done();
  });
});
describe('test exercice 4', () => {
  it('should return 0 when passed str = 27682697209', function (done) {
    let result = my_size_alpha(27682697209);
    assert.equal(result, 0, 'resultat correct');

    done();
  });
});

//Exercice 5
describe('test exercice 5', () => {
  it('should return ["d", "a", "e", "f", "a", "z", "f", "e", "z", "f", "z", "a"] when passed str = "asjhezhoefz"', function (done) {
    let result = my_array_alpha("daefazfezfza");
    expect(result).deep.to.equal(['d', 'a', 'e', 'f', 'a', 'z', 'f', 'e', 'z', 'f', 'z', 'a']);

    done();
  });
});

//Exercice 6
describe('test exercice 6', () => {
  it('should return 6 when passed arr = [1, 2, 3, 4, 5, 6]', function (done) {
    let result = my_length_array([1, 2, 3, 4, 5, 6]);
    assert.equal(result, 6, 'resultat correct');

    done();
  });
});
describe('test exercice 6', () => {
  it('should return 6 when passed arr = ["1", "2", "3", "4", "5", "6"]', function (done) {
    let result = my_length_array([1, 2, 3, 4, 5, 6]);
    assert.equal(result, 6, 'resultat correct');

    done();
  });
});

//Exercice 7
describe('test exercice 7', () => {
  it('should return NEGATIF when passed nbr = -1', function (done) {
    let result = my_is_posi_neg(-1);
    assert.equal(result, 'NEGATIF', 'resultat correct');

    done();
  });
});
describe('test exercice 7', () => {
  it('should return NEUTRAL when passed nbr = 0', function (done) {
    let result = my_is_posi_neg(0);
    assert.equal(result, 'NEUTRAL', 'resultat correct');

    done();
  });
})
describe('test exercice 7', () => {
  it('should return POSITIF when passed nbr = 1', function (done) {
    let result = my_is_posi_neg(1);
    assert.equal(result, 'POSITIF', 'resultat correct');

    done();
  });
})
describe('test exercice 7', () => {
  it('should return POSITIF when passed nbr = null', function (done) {
    let result = my_is_posi_neg(null);
    assert.equal(result, 'POSITIF', 'resultat correct');

    done();
  });
})
describe('test exercice 7', () => {
  it('should return POSITIF when passed nbr = undefined', function (done) {
    let result = my_is_posi_neg(undefined);
    assert.equal(result, 'POSITIF', 'resultat correct');

    done();
  });
})
